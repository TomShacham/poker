module Main where

import System.Random
import Data.List
import Control.Monad

main = do print $ (flop . drawHand . drawHand) gameStart

data Rank = Two | Three | Four | Five  | Six   | Seven | Eight | Nine 
                | Ten   | Jack  | Queen | King  | Ace  
                deriving (Show, Enum, Eq, Ord)

data Suit = Hearts | Diamonds | Spades | Clubs
            deriving (Show, Enum, Eq)

data Card = Card Rank Suit | Empty 
            deriving (Show, Ord)

type CardSet = [Card]
type Deck = CardSet
type Pocket = (Card, Card)
type Flop = (Card, Card, Card)
type Game = ([Pocket], Flop, Deck)
type Kicker = Card

data Hand = HighCard Kicker Kicker Kicker Kicker Kicker
          | Pair Card Kicker Kicker Kicker
          | TwoPair Card Card Kicker
          | Triple Card Kicker Kicker
          | Straight Card
          | Flush Card
          | FullHouse Card Card
          | FourOfAKind Card Kicker
          | StraightFlush Card deriving (Show, Ord, Eq)

instance Ord Suit where
  s1 <= s2 = False

instance Eq Card where
  Card r1 s1 == Card r2 s2 = r1 == r2


allCards :: CardSet
allCards = map toCard $ [(rank, suit) | rank <- [Two ..], suit <- [Hearts ..]]

toCard :: (Rank, Suit) -> Card
toCard (r, s) = Card r s

suit :: Card -> Suit
suit (Card r s) = s 

rank :: Card -> Rank
rank (Card r s) = r 

drawHand :: Game -> Game
drawHand (hands, flop, deck) = let (drawOne, deckOne) = randomCardForDeck deck
                                   (drawTwo, deckTwo) = randomCardForDeck deckOne
                               in (hands ++ [(drawOne, drawTwo)], flop, deckTwo)

flop :: Game -> Game
flop (hands, flop, deck) = let (drawOne, deckOne)     = randomCardForDeck deck
                               (drawTwo, deckTwo)     = randomCardForDeck deckOne
                               (drawThree, deckThree) = randomCardForDeck deckTwo
                           in  (hands, (drawOne, drawTwo, drawThree), deckThree)

randomCardForDeck :: Deck -> (Card, Deck)
randomCardForDeck deck = let draw = deck !! (fst $ randomR (0, length deck - 1) (mkStdGen 3))
                         in  (draw, deck \\ [draw])

isFlush :: [Card] -> Bool
isFlush cards = (<2) $ length $ nub $ map suit cards

isStraight :: [Card] -> Bool
isStraight cards =  all (==1) $ diff $ map (fromEnum . rank) cards 

isStraightFlush :: [Card] -> Bool
isStraightFlush cards = isStraight cards && isFlush cards

--isFourOfAKind :: [Card] -> [Int]
--isFourOfAKind cards = let ranks    = cardNumbers cards
--                          allRanks = map fromEnum [Two ..] 
--                      in  map (\card -> ranks) allRanks 

cardNumbers :: [Card] -> [Int]
cardNumbers cards = map (fromEnum . rank) $ sort cards

countIf :: (Int -> Bool) -> [Int] -> Int
countIf f []     = 0
countIf f (x:xs) = if f x
                     then 1 + countIf f xs
                     else countIf f xs

diff :: [Int] -> [Int]
diff [] = []
diff xs = zipWith (-) (tail xs) xs


gameStart :: Game
gameStart = ([], (Empty, Empty, Empty), allCards)















